﻿#include <stdio.h>
#include <string.h>
#define TOKEN_SIZE 20
char prog[1000],token[TOKEN_SIZE];
char ch;
int syn,p,m,n,sum;  /* p 是缓冲区prog的指针，m是token的指针*/
char *rwtable[6] = {"begin","if","then","while","do","end"};
void scaner();

char * source_file = "source.c";
int main()
{
    p = 0;
    printf("\n please input string :\n");

    FILE * thesource = freopen(source_file,"r",stdin);
    char ch = 0;
    do
    {
        //输入源程序字符串，送到缓冲区prog[p++]中；
        ch = getchar();
        prog[p++] = ch;
    }while(ch!='#');
    prog[p] = 0;
    p = 0;
   // printf("%s",prog);
    do
    {
        scaner();
        switch(syn)
        {
            case 11:
                {
                    printf("(%d,%d)\n",syn,sum);
                };break;
            case -1:
                {
                    printf("error!\n");
                };break;
            default:
                {
                    printf("(%d,%s)\n",syn,token);
                }
        }
    }while(syn != 0);
    fclose(thesource);
    return 0;
}

void scaner()
{
    int n;
    for(n = 0;n<TOKEN_SIZE;n++)token[n] = NULL;
    m = 0,sum = 0;
    //读入下一个字符；
    ch = prog[p++];
    while(ch == ' ')ch = prog[p++];//读入下一个字符；
    if ((ch>='a'&&ch<='z')||(ch>='A'&&ch<='Z'))//当ch是一个字母字符的时候
    {
        while(((ch>='a'&&ch<='z')||(ch>='A'&&ch<='Z'))||(ch >= '0' && ch <= '9'))
            {
                token[m++] = ch;// ch=> token
                ch = prog[p++];// 读入下一个字符
            }
            token[m++] = '\0';
            p--;//回退一个字符
            syn = 10;

            for (n = 0;n<6;n++)
            {

                if (strcmp(token,rwtable[n]) == 0)
                {
                    //给出syn的值；
                    syn = n+1;
                    break;
                }
            }
    }
    else if (ch >='0' && ch <= '9')//ch是数字字符
    {
        while (ch >= '0' && ch <= '9')//ch是数字字符
            {
                sum = sum * 10 + (ch-'0');
                ch = prog[p++];
            }
            //回退一个字符
            p--;
            syn = 11;
    }
    else
        switch(ch)
    {
    case '<':
        {
            m = 0;
            token[m++] = ch;
            ch = prog[p++];
            if (ch == '>')
            {
                syn = 21;
                token[m++] = ch;
            }
            else if (ch == '=')
            {
                syn = 22;
                token[m++] = ch;
            }
            else
            {
                syn = 20;
                p--;//回退一个字符
            }
        };break;
    case '>':
        {
            m = 0;
            token[m++] = ch;
            ch = prog[p++];//读入下一个字符
            if (ch == '=')
            {
                syn = 24;
                token[m++] = ch;
            }
            else
            {
                syn = 23;
                p--;//回退一个字符

            }
        };break;
    case ':':
        {
            m = 0;
            token[m++] = ch;
            ch = prog[p++];// 读入下一个字符
            if (ch == '=')
            {
                syn = 18;
                token[m++] = ch;

            }
            else
            {
                syn = 17;
                p--;
            }
        };break;
    case '+':
        {
            syn = 13;
            token[0] = ch;
        };break;
    case '-':
        {
            syn = 14;
            token[0] = ch;
        }break;
    case '*':
        {
            syn = 15;
            token[0] = ch;
        };break;
    case '/':
        {
            syn = 16;
            token[0] = ch;
        };break;
    case '=':
        {
            syn = 25;
            token[0] = ch;
        };break;
    case ';':
        {
            syn = 26;
            token[0] = ch;
        }break;
    case '(':
        {
            syn = 27;
            token[0] = ch;
        };break;
    case ')':
        {
            syn = 28;
            token[0] = ch;
        };break;
    case '#':
        {
            syn = 0;
            token[0] = ch;
        };break;

    default:syn = -1;
    }
}


int kk = 0;
void lrparser()
{
    void yucu();
    if (syn == 1)
    {
        scaner();
        yucu();
        if (syn == 6)
        {
            scaner();
            if (syn == 0 && (kk == 0))
            {
                printf("success!");
            }
            else
            {
                if (kk!= 1)
                {
                    printf("缺end!");
                    kk = 1;

                }
                else
                {
                    printf("begin!");
                    kk = 1;
                }
                return;
            }
        }
    }
}

void yucu()
{
    void statement();
    statement();
    while (syn == 26)
    {
        scaner();
        statement();
    }
    return;
}

void statement()
{
    void expression();
    if ( syn == 10)
    {
        scaner();
        if (syn == 18)
        {
            scaner();
            expression();

        }
        else
        {
            printf("赋值号错误！");
            kk = 1;
        }
    }
    else
    {
        printf("语句错误！");
        kk = 1;
    }
    return ;
}

void expression()
{
    void term();
    while ( syn == 13 || syn == 14)
    {
        scaner();
        term();
    }
    return;
}

void term()
{
    void factor();
    while ( syn == 15 || syn == 16)
    {
        scaner();
        factor();
    }
    return;
}

void factor()
{
    void expression();
    if (syn == 10 || syn == 11)
    {
        scaner();
    }
    else if ( syn == 27)
    {
        scaner();
        expression();
        if (syn == 28)
        {
            scaner();
        }
        else
        {
            printf("\")\"错误");
            kk = 1;
        }
    }
    else
    {
        printf("输出表达式错误！");
        kk = 1;
    }
    return;
}
